package negocio;

/**
 *
 * @author Bruno Machado
 */
public class Dado {
    
    /* ====================================
               ATRIBUTOS DA CLASSE
       ==================================== */
    
    private String palavra;
    
    /* ====================================
              CONSTRUTOR DA CLASSE
       ==================================== */
    
    
    public Dado(){
        
    }
    
    public Dado(String _palavra){
        this.setPalavra(_palavra);
    }

    /* ====================================
            MÉTODOS get E set DA CLASSE
       ==================================== */
    
    public String getPalavra() {
        return this.palavra;
    }

    public void setPalavra(String _palavra) {
        this.palavra = _palavra;
    }

    

}
