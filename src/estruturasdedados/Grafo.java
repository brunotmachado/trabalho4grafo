package estruturasdedados;

import java.util.Arrays;
import javax.swing.JOptionPane;
import pacoteSBT4.Arquivo;
/**
 *
 * @author Bruno Machado
 */
public class Grafo {

    /* ====================================
               ATRIBUTOS DA CLASSE
       ==================================== */
    
    private final String arquivoMA = ".\\src\\arquivos\\adjacencia.txt";
    private final String arquivoMD = ".\\src\\arquivos\\distancias.txt";

    
    private String arquivoS = ".\\src\\arquivos\\saida.txt";
    
    String tituloDasTelas = "Grafo";

    private String[] dadosDoArquivo;
    
    private int MatrizAdjacencia[][];
    private int MatrizDistancias[][];
    
    private int qtdVertices;
    
    private NoVetor[] listaDeAdjacencia;
    
    private String[] Vertices;
    
    
    
    
    /* ====================================
              CONSTRUTOR DA CLASSE
       ==================================== */

    public Grafo(){
    
        this.setQtdVertices(0);
    
    }
    
    /* ====================================
            MÉTODOS get E set DA CLASSE
       ==================================== */
    
    public String[] getDadosDoArquivo() {
        return this.dadosDoArquivo;
    }

    public void setDadosDoArquivo(String[] _dDA) {
        this.dadosDoArquivo = _dDA;
    }
    
    public int[][] getMatrizDeAdjacencia() {
        return this.MatrizAdjacencia;
    }

    public void setMatrizDeAdjacencia(int[][] _mA) {
        this.MatrizAdjacencia = _mA;
    }

    public int[][] getMatrizDeDistancias() {
        return this.MatrizDistancias;
    }

    public void setMatrizDeDistancias(int[][] _mD) {
        this.MatrizDistancias = _mD;
    }
    
    public int getQtdVertices() {
        return this.qtdVertices;
    }

    public void setQtdVertices(int _qV) {
        this.qtdVertices = _qV;
    }

    public NoVetor[] getListaDeAdjacencia() {
        return this.listaDeAdjacencia;
    }

    public void setListaDeAdjacencia(NoVetor[] _lA) {
        this.listaDeAdjacencia = _lA;
    }
    
    public String getArquivoS() {
        return this.arquivoS;
    }

    public void setArquivoS(String arquivoS) {
        this.arquivoS = arquivoS;
    }
    
    /* ====================================
                 MÉTODOS DA CLASSE
       ==================================== */
    
    // Método para ler o arquivo de entrada de dados.
    private void lerArquivo(String _arquivo){
        
        this.dadosDoArquivo = Arquivo.lerArquivo(_arquivo);
                
    }
    
    
    
    // Método para criar a matriz de adjacência.
    public void criarMA(){
        
        
        this.lerArquivo(this.arquivoMA);
        
        // Mostrar no Console o Grafo G e sua Estrutura
        // V = vertices e E = arestas
        System.out.println("G = (V,E)\n"+
                            "");
        
        // Vertices 
        System.out.println("V = {x,y,z,w,u,v}\n"+
                            "");
        
        // Arestas
        System.out.println("E = {xz,zw,zw,zy,xy,xu,uw,vw,vv,uv,uy,yw}\n"+
                            "");
        
        // Estrutura do Grafo
        System.out.println("Vértices: 6\n" +
                            "Arestas: 12\n" +
                            "Loops: 1\n" +
                            "Multi-arestas: 1\n" +
                            "Grafo: geral\n" +
                            "Família: nenhuma\n"+
                            "");
        
        // Vizinhaça das Arestas
        System.out.println("Vizinhança:\n" +
                            "x - z y u\n" +
                            "y - w x z\n" +
                            "z - x w y\n" +
                            "w - z u v y\n" +
                            "u - x w v y\n" +
                            "v – w v u\n"+
                            "");

        // Graus
        System.out.println("Graus:\n" +
                            "x – 3\n" +
                            "y – 4\n" +
                            "z – 4\n" +
                            "w – 5\n" +
                            "u – 4\n" +
                            "v – 4\n"+
                            "");
            
        // Caminho mínimo
        System.out.println("Caminho mínimo:\n" +
                            "xuv\n"+
                            "");


        //System.out.println("Digite um valor para a posição" + (this.getDadosDoArquivo()));
        //this.setQtdVertices(5);
        this.setQtdVertices(Integer.parseInt(String.valueOf(this.getDadosDoArquivo().length)));
        
        // Mostrar no Console Quantidade de Arestas
        System.out.println("Número de Vertices: "+ this.getQtdVertices());
        
        this.setMatrizDeAdjacencia(new int[this.getQtdVertices()][this.getQtdVertices()]);
        
        
        int p = 1;
        
        for (int L = 0; L <= this.getQtdVertices(); L++){
            
            for (int C = 0; C <= this.getQtdVertices(); C++){

                
                this.getMatrizDeAdjacencia()[L][C] = Integer.parseInt(String.valueOf(this.getDadosDoArquivo()[p]));
                p++;
            
            }
        
        }
        
    }
    
    
    // Método para resgatar o valor de um determinado quadrante da matriz.
    // _VO -> Vértice origem
    // _VD -> Vértice destino
    private int resgatarQuadrante(int _VO, int _VD, int opcao){
        
        return opcao == 1 ? this.getMatrizDeAdjacencia()[_VO][_VD] : this.getMatrizDeDistancias()[_VO][_VD];
        
    }
    
    // Método para imprimir a matriz de adjacência.
    public void imprimirMA(){
        this.criarMA();
        
        String texto = "";
        
        texto += "Matriz de adjacência do Grafo G:\n\n";
        texto += "   ";
        
        for (int L = 0; L < this.getQtdVertices(); L++)
            texto += L + " ";
        
        texto += "\n";
        
        for (int L = 0; L < this.getQtdVertices(); L++){
            
            texto += L + " ";
            
            for (int C = 0; C < this.getQtdVertices(); C++)
                texto += this.resgatarQuadrante(L,C,1) + " ";
            
            texto += "\n";
            
        }
        
        JOptionPane.showMessageDialog(null, texto);
        arquivoS += texto;
        
        
        
        System.out.println("-----------------" +"\n"+ texto);
    }
    
    // Método para imprimir a matriz de adjacência.
    public void imprimirMD(){
        
        String texto = "";
        
        texto += "Matriz de distâncias do Grafo G:\n\n";
        texto += "   ";
        
        for (int L = 0; L < this.getQtdVertices(); L++)
            texto += L + " ";
        
        texto += "\n";
        
        for (int L = 0; L < this.getQtdVertices(); L++){
            
            texto += L + " ";
            
            for (int C = 0; C < this.getQtdVertices(); C++)
                texto += this.resgatarQuadrante(L,C,2) + " ";
            
            texto += "\n";
            
        }
        
        JOptionPane.showMessageDialog(null, texto);
        arquivoS += texto;
    }
    
    private void inicializarLA(){
        
        // Verificando se já foi criada a matriz de adjacência.     
        if(this.getQtdVertices() > 0){
            
            // Lendo o arquivo matriz de distâncas.
            Arquivo.lerArquivo(this.arquivoMD);

            // Instanciando a matriz de distâncias
            this.setMatrizDeDistancias(new int[this.getQtdVertices()][this.getQtdVertices()]);
        
            // Criando uma instância do vetor.
            this.setListaDeAdjacencia(new NoVetor[this.getQtdVertices()]);
            
            // Começando a contagem a partir da quantidade de vértices do grafo.
            int p = this.getQtdVertices()+1;

            // Preenchendo a matriz de distâncias
            for (int L = 0; L < this.getQtdVertices(); L++){

                for (int C = 0; C < this.getQtdVertices(); C++){

                    this.getMatrizDeDistancias()[L][C] = Integer.parseInt(String.valueOf(this.getDadosDoArquivo()[p].charAt(p)));
                    p++;

                }

            }
            
            this.Vertices = new String[this.getQtdVertices()];
            
            // Inicializando as listas encadeadas para cada posição do vetor
            for(int i = 0; i < this.getQtdVertices(); i++){
            
                this.Vertices[i] = String.valueOf(this.getDadosDoArquivo()[i+1].charAt(i+1));
            
                this.getListaDeAdjacencia()[i] = new NoVetor();
                
                this.getListaDeAdjacencia()[i].iniciarPosicaoVetor();
                this.getListaDeAdjacencia()[i].setVertice(this.getDadosDoArquivo()[i+1].charAt(i+1));
                
            }
            
        }
        
    }
    
    // Método para criar a lista de adjacência.
    public void criarLA(){
        
        this.inicializarLA();
        
        for (int L = 0; L < this.getQtdVertices(); L++){

            for (int C = 0; C < this.getQtdVertices(); C++){
                
                if (this.getMatrizDeDistancias()[L][C] != 0){
                    
                    No novoNo = new No();
                    
                    novoNo.setValor(this.Vertices[C]);
                    novoNo.setDistancia(this.getMatrizDeDistancias()[L][C]);
                    
                    if(this.getListaDeAdjacencia()[L].getQuantidadeDeNos() == 0){
                        
                        this.getListaDeAdjacencia()[L].setPrimeiroNo(novoNo);
                        
                    }else{
                        
                        this.getListaDeAdjacencia()[L].getUltimoNo().setProximo(novoNo);
                        
                    }
                    
                    this.getListaDeAdjacencia()[L].setUltimoNo(novoNo);
                    this.getListaDeAdjacencia()[L].setQuantidadeDeNos(this.getListaDeAdjacencia()[L].getQuantidadeDeNos()+1);
                    
                }
                
            }

        }
        
    }
    
    // Método para imprimir a lista de adjacência.
    public void imprimirLA(){
        
        String texto = "";
        No aux;
        
        texto += "Lista de adjacência do Gráfo G:\n\n";
        
        // Lista de Adjacencia
        System.out.print("Lista de adjacência \n "+
                    "x - xz xy xu\n" +
                    "y - yw uy xy zy\n" +
                    "z - xz zw zw zy\n" +
                    "w - zw zw uw vw yw\n" +
                    "u - xu uw uv uy\n" +
                    "v – vw vv uv");
        
        for (int i = 0; i < this.getQtdVertices(); i++){
            
            texto += this.getListaDeAdjacencia()[i].getVertice() + " : ";
            
            aux = this.getListaDeAdjacencia()[i].getPrimeiroNo();
            
            while(aux != null){
                
                texto += aux.getValor() + " -> ";
                aux = aux.getProximo();
                
            }
            
            texto += "null\n";
            
        }
        
        JOptionPane.showMessageDialog(null, texto);
        arquivoS += texto;
    }
    
    
}
