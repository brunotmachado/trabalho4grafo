package pacoteSBT4;

import estruturasdedados.Grafo;

/**
 *
 * @author Bruno Machado
 */
public class Operacoes {

    /* ====================================
                ATRIBUTOS DA CLASSE
       ==================================== */
    
    private final String arquivoAdjacencia = ".\\src\\arquivos\\adjacencia.txt";
    private final String arquivoDistancias = ".\\src\\arquivos\\distancias.txt";
    private final String arquivoSaida      = ".\\src\\arquivos\\saida.txt";
    
    String tituloDasTelas = "Grafo";

    private String[] dadosDoArquivoA; // Arquivo de adjacencia
    private String[] dadosDoArquivoD; // Arquivo de distâncias
    private String[] dadosDoArquivoS; // Arquivo de saída
    
    /* ====================================
         ATRIBUTOS DE INSTÂNCIA DA CLASSE
       ==================================== */

    // Atributo para manipular o grafo G.
    private Grafo G = new Grafo();

    /* ====================================
            MÉTODOS get E set DA CLASSE
       ==================================== */

    public Grafo getG() {
        return this.G;
    }

    private void setG(Grafo _g) {
        this.G = _g;
    }
    
    public String[] getDadosDoArquivoA() {
        return this.dadosDoArquivoA;
    }

    public void setDadosDoArquivoA(String[] dadosDoArquivoA) {
        this.dadosDoArquivoA = dadosDoArquivoA;
    }

    public String[] getDadosDoArquivoD() {
        return this.dadosDoArquivoD;
    }

    public void setDadosDoArquivoD(String[] dadosDoArquivoD) {
        this.dadosDoArquivoD = dadosDoArquivoD;
    }

    public String[] getDadosDoArquivoS() {
        return this.dadosDoArquivoS;
    }

    public void setDadosDoArquivoS(String[] dadosDoArquivoS) {
        this.dadosDoArquivoS = dadosDoArquivoS;
    }

    /* ====================================
               OPERAÇÕES DA CLASSE
       ==================================== */
    
    // Método para criar registrar uma nova instância do objeto grafo.
    public void criarGrafo(){
        
        this.setG(new Grafo());
        
        this.lerArquivos();
        
        // Criar Matriz de Adjacências
        G.criarMA();
        
        // Criar Lista de Adjacências4  
        G.criarLA();
        
    }
    
    // Método que implementa o método para ler os arquivos de entrada de dados.
    public void lerArquivos(){
        
        
        
        dadosDoArquivoA = Arquivo.lerArquivo(this.arquivoAdjacencia);
        dadosDoArquivoD = Arquivo.lerArquivo(this.arquivoDistancias);
        dadosDoArquivoS = Arquivo.lerArquivo(this.arquivoSaida);
        
        
        
    }
    
    // Método que implementa o método para gerar o relatório de saída de dados.
    public void relatorioEmArquivo(){
        
        String texto = null;
        
        Arquivo.gerarRelatorio(this.arquivoSaida,texto);
        
        // Imprimir Matriz de Adjacência
        G.imprimirMA();
        
        // Imprimir Matriz de Distancias
        G.imprimirMD();
        
        // Imprimir Lista de Adjacência
        G.imprimirLA();
        
    }
    
    // Método para registrar null ao objeto G.
    public void sair(){
        
        this.setG(null);
        
    }
    
}
